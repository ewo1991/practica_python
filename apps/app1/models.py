from django.db import models

class facultades(models.Model):
    descripcionfacultad = models.CharField(max_length=80)
    estadofacultad      = models.BooleanField(default=True)

    def __str__(self):
        return '%s' % (self.descripcionfacultad)

class escuelaprofesional(models.Model):
    descripcionescuela = models.CharField(max_length=100)
    idfacultad        = models.ForeignKey(facultades)
    estadoescuela      = models.BooleanField(default=True)
    def __str__(self):
        return '%s' % (self.descripcionescuela)

class plancurricular(models.Model):
    descripcionplan = models.CharField(max_length=200)
    idescuela         = models.ForeignKey(escuelaprofesional)
    estadoplan      = models.BooleanField(default=True)
    def __str__(self):
        return '%s' % (self.descripcionplan)

class cursos(models.Model):
    descripcioncurso= models.CharField(max_length=255)
    creditos        = models.IntegerField()
    tipocurso       = models.CharField(max_length=1)
    ciclo           = models.CharField(max_length=8)
    idplan            = models.ForeignKey(plancurricular)
    estadocurso     = models.BooleanField(default=True)
    def __str__(self):
        return '%s' % (self.descripcioncurso)

class requisitos(models.Model):
    descripcionrequisito= models.CharField(max_length=200)
    idcurso               = models.ForeignKey(cursos)
    estadorequisito     = models.BooleanField(default=True)
    def __str__(self):
        return '%s' % (self.descripcionrequisito)