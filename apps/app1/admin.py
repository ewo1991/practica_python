from django.contrib import admin
from .models import * 

class facultad(admin.ModelAdmin):
    list_display = ('descripcionfacultad', 'estadofacultad')
    

class escuela(admin.ModelAdmin):
    list_display = ('descripcionescuela', 'idfacultad', 'estadoescuela')

class plancuricular(admin.ModelAdmin):
    list_display = ('descripcionplan', 'idescuela', 'estadoplan')

class curso(admin.ModelAdmin):
    list_display = ('descripcioncurso', 'creditos', 'tipocurso', 'ciclo', 'idplan', 'estadocurso')
    search_fields = ('id','descripcioncurso','creditos','ciclo','tipocurso')
    list_filter = ('idplan', 'tipocurso')
    fields      =('descripcioncurso','creditos','tipocurso','ciclo','idplan')
    ordering    = ('id',)

class requisito(admin.ModelAdmin):
    list_display = ('descripcionrequisito', 'idcurso', 'estadorequisito')

# Register your models here.
admin.site.register(facultades, facultad)
admin.site.register(escuelaprofesional, escuela)
admin.site.register(plancurricular, plancuricular)
admin.site.register(cursos, curso)
admin.site.register(requisitos, requisito)



